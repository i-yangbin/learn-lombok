package com.study.demo.learn.lombok.example;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UseDataTest {

    @Test
    public void testUseData() {

        // @Data: 注解在类上, 将类提供的所有属性都添加 get、set 方法, 并添加 equals、canEqual、hashCode、toString 方法.

        UseData example = new UseData();

        example.setId(1212L);
        example.setName("杨斌");
        example.setAge(18);

        Assert.assertEquals(Long.valueOf(1212L), example.getId());
        Assert.assertEquals("杨斌", example.getName());
        Assert.assertEquals(Integer.valueOf(18), example.getAge());

        Assert.assertFalse(example.equals(new UseData()));
        Assert.assertTrue(example.canEqual(new UseData()));

        log.info("【@Data】hashCode: {}", example.hashCode());
        log.info("【@Data】toString: {}", example.toString());
    }
}