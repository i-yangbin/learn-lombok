package com.study.demo.learn.lombok.example;


import lombok.Getter;

/**
 * @author yangbin
 **/
@Getter
public class UseGetter {

    private Long id;

    private String name;
}
