package com.study.demo.learn.lombok.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author yangbin
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class UseAccessorsTest {

    @Test
    public void testUseAccessors() {

        // @Accessors(chain = true) 使用链式设置属性, set 方法返回的是 this 对象.

        UseAccessors example = new UseAccessors();

        example.setId(1212L).setName("杨斌").setAge(18);
    }
}