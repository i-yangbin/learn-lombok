package com.study.demo.learn.lombok.example;

import lombok.Data;

/**
 * @author yangbin
 **/
@Data
public class UseData {

    private Long id;

    private String name;

    private Integer age;
}

