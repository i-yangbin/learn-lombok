package com.study.demo.learn.lombok.example;

import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author yangbin
 **/
@Setter
@Accessors(chain = true)
public class UseAccessors {

    private Long id;

    private String name;

    private Integer age;
}
