package com.study.demo.learn.lombok.example;

import lombok.NonNull;
import lombok.Setter;

/**
 * @author yangbin
 **/
@Setter
public class UseNotNull {

    private Long id;

    @NonNull
    private String name;
}
