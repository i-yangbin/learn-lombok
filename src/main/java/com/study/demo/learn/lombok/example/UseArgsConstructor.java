package com.study.demo.learn.lombok.example;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * @author yangbin
 **/
public class UseArgsConstructor {
}

@NoArgsConstructor
class UseNoArgsConstructor {
    private Long id;
    private String name;
}

@AllArgsConstructor
class UseAllArgsConstructor {
    private Long id;
    private String name;
}

@RequiredArgsConstructor
class UseRequiredArgsConstructor {
    private Long id;
    @NonNull
    private String name;
}